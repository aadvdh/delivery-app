<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderStoreRequest;
use App\Http\Requests\OrderUpdateRequest;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Models\CustomerDetails;
use App\Models\Order;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class OrderController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\OrderCollection
     */
    public function index(Request $request)
    {
        $orders = Shop::find(Auth::id())->orders;
        return Inertia::render('Orders', [
            'orders' => $orders->load('products')
        ]);


    }

    /**
     * @param \App\Http\Requests\OrderStoreRequest $request
     * @return \App\Http\Resources\OrderResource
     */
    public function store(Request $request)
    {



        $order = new Order($request->validate([
            'customer_name' => 'required',
            'phone_number' => 'required',
            'address' => 'required|max:100',
            'zip_code' => 'required|max:6|min:6',
            'email' => 'required',
            'city' => 'required',
            'notes' => 'nullable'

        ]));
        $order->save();

        foreach($request->products as $product) {
            $order->products()->attach(
                $product['id'], ['quantity' => $product['quantity']]
            );

        };


        return Redirect::route('/');
        //return new OrderResource($order);
    }


}

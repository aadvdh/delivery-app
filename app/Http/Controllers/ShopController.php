<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Shop;
use App\Settings\OrderSettings;
use App\Settings\ShopSettings;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ShopController extends Controller
{
    public function show(OrderSettings $orderSettings, ShopSettings $shopSettings) {
        $categories = Category::where('active',true)->orderBy('order')->get();
        return Inertia::render('Shop', [
            'categories' => $categories->map(function ($category) {
                return [
                    'id' => $category->id,
                    'name' => $category->name,
                    'products' => $category->products
                ];
            }),
            'shopSettings' => $shopSettings->toArray(),
            'orderSettings' => $orderSettings->toArray()
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderStoreRequest;
use App\Http\Requests\OrderUpdateRequest;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Models\Category;
use App\Models\CustomerDetails;
use App\Models\Order;
use App\Models\Shop;
use App\Settings\OrderSettings;
use App\Settings\ShopSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Inertia\Inertia;

class CategoryController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\OrderCollection
     */
    public function index(OrderSettings $orderSettings, ShopSettings $shopSettings)
    {
        $categories = Category::where('active',true)->orderBy('order')->get();
        return Inertia::render('Shop', [
            'categories' => $categories->map(function ($category) {
                return [
                    'id' => $category->id,
                    'name' => $category->name,
                    'products' => $category->products
                ];
            }),
            'shopSettings' => $shopSettings->toArray(),
            'orderSettings' => $orderSettings->toArray()
        ]);


    }

    public function edit(Category $category) {

        return Inertia::render('Categories/Edit', [
            'category' => $category
        ]);
    }

    /**
     * @param \App\Http\Requests\OrderStoreRequest $request
     * @return \App\Http\Resources\OrderResource
     */
    public function store(Shop $shop, Request $request)
    {

        $order = $shop->orders()->create([]);
        $order->customerDetails()->create(['name' => $request->name, 'contactNumber' => $request->contactNumber]);
       /*  $order->products()->attach([
            $request->products
        ]); */

        foreach($request->products as $product) {
            $order->products()->attach(
                $product['id'], ['quantity' => $product['quantity']]
            );

        };
        $string = $order->toString();
        $whatsappUrl = "https://api.whatsapp.com/send?phone=918800671610&text=" . urlencode($string);

        return Inertia::location($whatsappUrl);
        //return new OrderResource($order);
    }


}

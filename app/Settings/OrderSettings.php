<?php

namespace App\Settings;

Use Spatie\LaravelSettings\Settings;
class OrderSettings extends Settings
{

    public int $tax_rate;
    public float $minimum_order_delivery;
    public string $minimum_order_free_delivery;
    public string $delivery_fee;

    public static function group(): string {
        return "order";
    }



}

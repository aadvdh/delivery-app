<?php

namespace App\Settings;

Use Spatie\LaravelSettings\Settings;
class ShopSettings extends Settings
{

    public string $name;
    public string $description;
    public string $logo_url;
    public string $phone_number;
    public string $zip_code;
    public string $city;
    public string $street;

    public static function group(): string {
        return "shop";
    }



}

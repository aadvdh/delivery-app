<?php

namespace App\Providers;
use Illuminate\Support\Facades\File; // <-- added
use Illuminate\Support\ServiceProvider;
use Inertia\Inertia; // <-- added
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {


    }
}

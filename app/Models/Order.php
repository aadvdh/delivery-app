<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Inertia\Inertia;

class Order extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['customer_name', 'address', 'zip_code','city','email', 'phone_number', 'notes'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
    ];
    public function totalPrice() {
        $products = $this->products;
        $total = 0;
        foreach($products as $product) {
            $total = $total + ($product->price * $product->pivot->quantity);
        }
        return $total;
    }





    public function products()
    {
        return $this->belongsToMany(\App\Models\Product::class)->withPivot('quantity');
    }


}

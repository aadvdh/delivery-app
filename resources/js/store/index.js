import Vue from "vue";
import Vuex from "vuex";
import cart from "./cart.module";
import shop from "./shop.module";
import delivery from "./delivery.module";
Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        cart,
        shop,
        delivery
    }
});

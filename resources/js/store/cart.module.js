import {
    ADD_TO_CART,
    REMOVE_ENTIRE_FROM_CART,
    REMOVE_FROM_CART
} from "./actions.type";
import {
    ADD_INSTRUCTIONS,
    ADD_PRODUCT,
    DECREASE_QUANTITY,
    INCREASE_QUANTITY,
    REMOVE_INSTRUCTIONS,
    REMOVE_PRODUCT
} from "./mutations.type";

const initialState = {
    products: []
};

export const state = { ...initialState };

export const actions = {
    [ADD_TO_CART]({ commit, getters }, product, quantity = 1) {
        // avoid extronuous network call if article exists
        let item = getters.product(product.id);
        if (item) {
            commit(INCREASE_QUANTITY, product, quantity);
        } else {
            commit(ADD_PRODUCT, product);
        }
    },
    [REMOVE_FROM_CART]({ commit, getters }, product, quantity = 1) {
        let item = getters.product(product.id);
        if (item) {
            item.quantity - quantity <= 0
                ? commit(REMOVE_PRODUCT, product)
                : commit(DECREASE_QUANTITY, product, quantity);
        }
    },
    [REMOVE_ENTIRE_FROM_CART]({ commit, getters }, product) {
        commit(REMOVE_PRODUCT, product);
    },
    [ADD_INSTRUCTIONS]({ commit }, { product, instructions } = payload) {
        console.log(instructions);
        commit(ADD_INSTRUCTIONS, product, instructions);
    },
    [REMOVE_INSTRUCTIONS]({ commit }, product) {
        commit(REMOVE_INSTRUCTIONS, product);
    }
};

/* eslint no-param-reassign: ["error", { "props": false }] */
export const mutations = {
    [ADD_PRODUCT](state, product) {
        state.products = [...state.products, { ...product, quantity: 1 }];
        return product;
    },
    [REMOVE_PRODUCT](state, product) {
        state.products = state.products.filter(item => {
            return item.id != product.id;
        });
    },
    [INCREASE_QUANTITY](state, product, quantity = 1) {
        state.products = state.products.map(item => {
            if (product.id == item.id) {
                return { ...item, quantity: item.quantity + quantity };
            }
            return item;
        });
    },
    [DECREASE_QUANTITY](state, product, quantity = 1) {
        state.products = state.products.map(item => {
            if (product.id == item.id) {
                return { ...item, quantity: item.quantity - quantity };
            }
            return item;
        });
    },
    [ADD_INSTRUCTIONS](state, product, instructions) {
        state.products = state.products.map(item => {
            console.log(product);
            if (product.id == item.id) {
                console.log(true);
                return { ...item, instructions };
            }
            return item;
        });
    },
    [REMOVE_INSTRUCTIONS](state, product) {
        state.products = state.products.map(item => {
            if (product.id == item.id) {
                delete item.instructions;
            }
            return item;
        });
    }
};

const getters = {
    product: state => id => {
        return state.products.find(product => product.id == id);
    },
    products(state) {
        return state.products;
    },
    subTotal(state) {
        return state.products.reduce(
            (prev, curr) => prev + curr.quantity * curr.price,
            0
        );
    },
    productCount(state) {
        return state.products.reduce((prev, curr) => prev + curr.quantity, 0);
    },
    productIds(state) {
        return state.products.map(product => {
            console.log(product);
            return { id: product.id, quantity: product.quantity };
        });
    }
};

export default {
    state,
    actions,
    mutations,
    getters
};

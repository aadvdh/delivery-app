export const ADD_PRODUCT = "addProduct";
export const REMOVE_PRODUCT = "removeProduct";
export const INCREASE_QUANTITY = "increaseQuantity";
export const DECREASE_QUANTITY = "decreaseQuantity";
export const ADD_INSTRUCTIONS = "addInstructions";
export const REMOVE_INSTRUCTIONS = "removeInstructions";
export const SET_SEARCH_QUERY = "setSearchQuery";
export const SET_ERRORS = "setErrors";

export const ADD_TO_CART = "addToCart";
export const REMOVE_FROM_CART = "removeFromCart";
export const ADD_INSTRUCTIONS = "addInstructions";
export const REMOVE_INSTRUCTIONS = "removeInstructions";
export const REMOVE_ENTIRE_FROM_CART = "removeEntireFromCart";
export const UPDATE_SEARCH_QUERY = "updateSearchQuery";

import { UPDATE_SEARCH_QUERY } from "./actions.type";
import { SET_SEARCH_QUERY } from "./mutations.type";

const initialState = {
    searchQuery: ""
};

export const state = { ...initialState };

export const actions = {
    [SET_SEARCH_QUERY]({ commit }, query) {
        // avoid extronuous network call if article exists
        commit(UPDATE_SEARCH_QUERY, query);
    }
};

/* eslint no-param-reassign: ["error", { "props": false }] */
export const mutations = {
    [UPDATE_SEARCH_QUERY](state, query) {
        state.searchQuery = query;
    }
};

const getters = {
    searchQuery: state => {
        return state.searchQuery;
    }
};

export default {
    state,
    actions,
    mutations,
    getters
};

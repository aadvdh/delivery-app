import { getField, updateField } from "vuex-map-fields";
import { SET_ERRORS } from "./mutations.type";
const initialState = {
    name: "Aad van den Hoogenband",
    contactNumber: "0636162394",
    address: "Pr Marijkestraat 9",
    zipCode: "4671GB",
    city: "Dinteloord",
    email: "vandenhoogenbandaad@gmail.com",
    errors: {}
};

export const state = { ...initialState };

export const actions = {};

/* eslint no-param-reassign: ["error", { "props": false }] */
export const mutations = {
    updateField,
    [SET_ERRORS](state, errors) {
        state.errors = errors;
    }
};

const getters = {
    getField,
    form(state) {
        return state;
    }
};

export default {
    state,
    actions,
    mutations,
    getters
};

export default (value, decimals) => {
    if (isNaN(value)) {
        return value;
    }
    return value.toFixed(decimals);
};

require("./bootstrap");

require("moment");

import Vue from "vue";

import { InertiaApp } from "@inertiajs/inertia-vue";
import { InertiaForm } from "laravel-jetstream";
import PortalVue from "portal-vue";
import store from "./store";
import VueScrollactive from "vue-scrollactive";
import toFixedFilter from "./common/toFixed.filter";

Vue.mixin({ methods: { route } });
Vue.use(InertiaApp);
Vue.use(InertiaForm);
Vue.use(PortalVue);
Vue.use(VueScrollactive);
Vue.filter("toFixed", toFixedFilter);
const app = document.getElementById("app");
Vue.prototype.$route = (...args) => route(...args).url();
new Vue({
    store,
    render: h =>
        h(InertiaApp, {
            props: {
                initialPage: JSON.parse(app.dataset.page),
                resolveComponent: name => require(`./Pages/${name}`).default
            }
        })
}).$mount(app);

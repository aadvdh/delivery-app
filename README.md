## About

Kleine Single page shop in de stijl van thuisbezorgd.nl
Gebouwd met Laravel, Inertia, Vue en Tailwindcss.

## Setup Dev

Voor snelle dev omgeving wordt [Lando](https://lando.dev) gebruikt. Dit is een kleine abstractie laag bovenop Docker om gebruik makkelijker te maken.  
Backend:  
`lando start`

Laravel Mix voor frontend assets.  
Frontend:  
`npm run watch`

## TODO

1. Admin backend
2. Betalingen
3. Order validatie in backend

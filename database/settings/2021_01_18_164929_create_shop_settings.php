<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class CreateShopSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('shop.name', "McNotDonalds");
        $this->migrator->add('shop.description', "Just good ol' food");
        $this->migrator->add('shop.logo_url', "/logo");
        $this->migrator->add('shop.phone_number', "106-523212");
        $this->migrator->add('shop.zip_code', "4323KD");
        $this->migrator->add('shop.city', "Rotterdam");
        $this->migrator->add('shop.street', "Willemskade 300");
    }
}

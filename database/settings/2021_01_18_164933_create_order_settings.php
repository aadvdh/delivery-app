<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class CreateOrderSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('order.tax_rate', 9);
        $this->migrator->add('order.minimum_order_delivery', 15.00);
        $this->migrator->add('order.minimum_order_free_delivery', 20.00);
        $this->migrator->add('order.delivery_fee', 1.50);
    }
}
